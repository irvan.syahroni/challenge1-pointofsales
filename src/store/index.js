import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isVerified: false,
    filterUsed: 'all',
    cart: [],
    checkouted: [],
  },
  mutations: {
    // change product filter
    changeFilter(state, payload) {
      state.filterUsed = payload;
    },
    // add product to cart
    addCart(state, payload) {
      const index = state.cart.findIndex(arr => arr.id === payload.id);
      if (index === -1) state.cart.push({ ...payload, amount: 1 });
      else state.cart[index].amount += 1;
      // TODO: check if existed if existed add amount instead
    },
    addAmount(state, payload) {
      const index = state.cart.findIndex(arr => arr.id === payload.id);
      state.cart[index].amount += 1;
    },
    reduceAmount(state, payload) {
      const index = state.cart.findIndex(arr => arr.id === payload.id);
      if (state.cart[index].amount >= 2) state.cart[index].amount -= 1;
      else state.cart.splice(index, 1);
    },
    // delete profuct from cart
    removeItem(state, payload) {
      const index = state.cart.findIndex(arr => arr.id === payload.id);
      state.cart.splice(index, 1);
    },
    finishOrder(state) {
      state.cart = [];
    },
    loginSuccess(state) {
      state.isVerified = true;
    },
    logout(state) {
      state.isVerified = false;
    },
  },
  actions: {
    login() {},
  },
  modules: {},
});

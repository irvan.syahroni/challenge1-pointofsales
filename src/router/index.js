import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/index';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {
      requiresAuth: true,
    },
    component: Home,
  },
  {
    path: '/checkout',
    name: 'Checkout',
    meta: {
      requiresAuth: true,
    },
    component: () => import('../views/Checkout.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(value => value.meta.requiresAuth)) {
    if (!store.state.isVerified) {
      next({
        path: '/login',
      });
    } else {
      next();
    }
  } else if (to.path === '/login' && store.state.isVerified) {
    next({
      path: from.path,
      replace: true,
    });
  } else {
    next();
  }
});

export default router;
